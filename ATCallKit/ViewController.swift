//
//  ViewController.swift
//  ATCallKit
//
//  Created by Dejan on 19/05/2019.
//  Copyright © 2019 agostini.tech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func receive(_ sender: Any) {
        ATCallManager.shared.incommingCall(from: "jane@example.com", delay: 0)
    }
    
    @IBAction func receiveWithDelay(_ sender: Any) {
        ATCallManager.shared.incommingCall(from: "jerry@example.com", delay: 5)
    }
    
    @IBAction func send(_ sender: Any) {
        ATCallManager.shared.outgoingCall(from: "me@example.com", connectAfter: 5)
    }
}

